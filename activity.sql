SELECT customerName
FROM customers
WHERE country = "Philippines";

SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

SELECT firstName, lastName 
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName
FROM customers
WHERE state is NULL;

SELECT firstName, lastName, email 
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND creditLimit > 3000;

SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";

SELECT orderNumber
FROM orders
WHERE comments LIKE "%DHL%";

SELECT productLine
FROM productlines
WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country 
FROM customers;

SELECT DISTINCT status
FROM orders;

SELECT customerName, country
FROM customers
WHERE country IN ("USA", "FRANCE", "CANADA");

SELECT employees.firstName, employees.lastName,offices.city
FROM employees JOIN offices
ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

SELECT customers.customerName
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE firstName = "Leslie" AND lastName = "Thompson";

SELECT products.productName,customers.customerName
FROM customers JOIN orders
ON customers.customerNumber = orders.customerNumber
JOIN orderdetails
ON orders.orderNumber = orderdetails.orderNumber
JOIN products
ON orderdetails.productCode = products.productCode
WHERE customers.customerName = "Baane Mini Imports";


SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
ON  employees.officeCode = offices.officeCode;

SELECT lastName, firstName
FROM employees 
WHERE reportsTo = 1056;

SELECT productName, MAX(MSRP)
FROM products
GROUP BY productName
ORDER BY MAX(MSRP);

SELECT COUNT(country)
FROM customers
WHERE country = "UK";

SELECT productName,productLine,COUNT(productLine)
FROM products 
GROUP BY productLine;


SELECT customerName, COUNT(salesRepEmployeeNumber)
FROM customers
GROUP BY salesRepEmployeeNumber;

SELECT productName, quantityInStock
FROM products
WHERE productLine = "planes" AND quantityInStock < 1000;








